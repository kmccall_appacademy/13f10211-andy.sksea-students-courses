require_relative 'student'

class Course
  
  attr_reader :name, :department, :credits, :students, :days, :time_block
  
  def initialize(name, department, credits, days=nil, time_block=nil)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end
  
  def add_student(student)
    student.enroll(self)
  end
  
  # def include?(student)
  #   return @students.include?(student)
  # end
  
  def <<(student)
    @students << student
  end

  def conflicts_with?(course)
    @time_block == course.time_block && @days.any? { |d| course.days.include?(d) }
  end
  
end
