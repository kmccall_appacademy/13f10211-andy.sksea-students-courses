# require './course' is evaluated at run time, and will only work if
# working directory is lib.
require_relative 'course'

class Student
  
  attr_reader :courses, :first_name, :last_name
  
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end
  
  def name
    "#{@first_name} #{@last_name}"
  end
  
  def enroll(course)
    raise ArgumentError, "Can't enroll in course with conflicting time blocks" if @courses.any? { |c| c.conflicts_with?(course) }
    if !course.students.include?(self)
      course.students << self
      @courses << course
    end
    nil
  end
  
  def course_load
    cl = Hash.new(0)
    @courses.each { |c| cl[c.department] += c.credits }
    cl
  end
    
end
